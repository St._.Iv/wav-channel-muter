#include <stdio.h>
#include <stdlib.h>
#include "wav_header.h"
#include <string.h>


//----------------------------------------
//DESCRIPTION: Read a WAV file and extract the header and data size information
//
//PARAMETERS: file_path - path to the input WAV file, silent - a flag to control output
//
//RETURN VALUE: A WavData struct containing the number of channels and the size of the sound content
//----------------------------------------
WavData read_wav_file(const char *file_path, int silent) {
    if (!silent) {
        printf("[read_wav_file] Reading WAV file: %s\n", file_path);
    }

    FILE *file = fopen(file_path, "rb");

    if (!file) {
        if(!silent) {
            printf("[read_wav_file] Error opening WAV file.\n");
        }
        exit(1);
    }

    WavHeader header;
    fread(&header, 1, sizeof(header), file);
    fclose(file);


    if (strncmp(header.chunkID, "RIFF", 4) != 0 || strncmp(header.format, "WAVE", 4) != 0) {
        printf("[read_wav_file] The provided file is not a WAV file\n");
        exit(1);
    }


    WavData wav_data;
    wav_data.channels = header.numChannels;
    wav_data.size = header.subchunk2Size;
    return wav_data;
}

//----------------------------------------
//DESCRIPTION: Display the size of the sound content in the specified unit
//
//PARAMETERS: size - the size of the sound content in bytes, unit - the unit to display the size ('K', 'M', or 'B')
//
//RETURN VALUE: None
//----------------------------------------
void display_size(uint32_t size, char unit) {
    double converted_size;


    if (unit == 'B' || (unit == ' ' && size < 1024)) {
        printf("[display_size] The sound content is %.2u B\n",size);
    }
    else if (unit == 'K' || (unit == ' ' && size < 1024 * 1024)) {
        converted_size = (double) size / 1024;
        printf("[display_size] The sound content is %.2f KB\n", converted_size);
    }
    else if (unit == 'M' || unit == ' ') {
        converted_size = (double) size / (1024 * 1024);
        printf("[display_size] The sound content is %.2f MB\n", converted_size);
    }
}