#ifndef WAV_HEADER_H
#define WAV_HEADER_H

#include <stdint.h>

// Define the WavHeader structure based on the WAV file header
typedef struct {
    char     chunkID[4];
    uint32_t chunkSize;
    char     format[4];
    char     subchunk1ID[4];
    uint32_t subchunk1Size;
    uint16_t audioFormat;
    uint16_t numChannels;
    uint32_t sampleRate;
    uint32_t byteRate;
    uint16_t blockAlign;
    uint16_t bitsPerSample;
    char     subchunk2ID[4];
    uint32_t subchunk2Size;
} WavHeader;

// stores the number of channels and the size of the sound content
typedef struct {
    int channels;
    uint32_t size;
} WavData;

WavData read_wav_file(const char *filepath, int silent);

void display_size(uint32_t size, char unit);

#endif


