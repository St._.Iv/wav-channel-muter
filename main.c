#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "wav_header.h"
#include "mute_channel.h"

    //----------------------------------------
    //DESCRIPTION: Entry point of the program, processes command-line arguments, and calls necessary functions
    //
    //PARAMETERS: argc - the number of command-line arguments, argv - an array of pointers to the arguments
    //
    //RETURN VALUE: EXIT_SUCCESS or EXIT_FAILURE
    //----------------------------------------
    int main(int argc, char *argv[]) {
        int silent = 0;
        char *input_file = NULL;
        char unit = ' ';
        int opt;

        while ((opt = getopt(argc, argv, "sf:u:")) != -1) {
            switch (opt) {
                case 's':
                    silent = 1;
                    break;
                case 'f':
                    input_file = strdup(optarg);
                    break;
                case 'u':
                    if (optarg[0] == 'K' || optarg[0] == 'M' || optarg[0] == 'B') {
                        unit = optarg[0];
                    } else {
                        printf("Invalid unit provided. Please use 'K', 'M', or 'B'.\n");
                        exit(EXIT_FAILURE);
                    }
                    break;
                default:
                    printf("Usage: %s [-s] [-f input_file] [-u unit]\n", argv[0]);
                    exit(EXIT_FAILURE);
            }
        }

        if (!input_file) {
            printf("Please enter the input WAV file path: ");
            char file_path[256];
            scanf("%255s", file_path);
            input_file = strdup(file_path);
        }

        WavData wav_data = read_wav_file(input_file, silent);

        if (!silent) {
            printf("[main] Number of channels: %d\n", wav_data.channels);
            display_size(wav_data.size, unit);
        }

        if (wav_data.channels == 2) {
            char channel[8];
            int channel_to_mute = -1;
            do {
                printf("Enter the channel to mute (left or right): ");
                scanf("%7s", channel);

                if (strcmp(channel, "left") == 0) {
                    channel_to_mute = 0;
                } else if (strcmp(channel, "right") == 0) {
                    channel_to_mute = 1;
                }
            } while (channel_to_mute == -1);


            char output_file[256];
            snprintf(output_file, sizeof(output_file), "%s-%s-channel-muted.wav",
                     input_file, channel_to_mute == 0 ? "left" : "right");


            int replaced_samples = mute_channel(input_file, output_file, channel_to_mute, silent);


            if (!silent) {
                printf("[main] Output file: %s\n", output_file);
                printf("[main] %d samples on the %s channel were replaced with 0.\n",
                       replaced_samples, channel_to_mute == 0 ? "left" : "right");
            }
        } else {
            if (!silent) {
                printf("[main] The file is not stereo channel. No muting was performed.\n");
            }
        }

        free(input_file);

        return 0;
    }
