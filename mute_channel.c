#include <stdio.h>
#include <stdlib.h>
#include "wav_header.h"
#include "mute_channel.h"

//----------------------------------------
//DESCRIPTION: Mute a specified channel in a WAV file and create a new output file with the muted channel
//
//PARAMETERS: input_file - path to the input WAV file, output_file - path to the output WAV file,
//            channel_to_mute - the index of the channel to mute (0 for left, 1 for right), silent - a flag to control verbose output
//
//RETURN VALUE: The number of samples replaced with silence in the muted channel
//----------------------------------------
int mute_channel(const char *input_file, const char *output_file, int channel_to_mute, int silent) {
    FILE *in = fopen(input_file, "rb");
    FILE *out = fopen(output_file, "wb");
    int replaced_samples = 0;

    if (!in || !out) {
        if (!silent) {
            printf("[mute_channel] Error opening input or output file.\n");
        }
        exit(1);
    }

    WavHeader header;
    fread(&header, 1, sizeof(header), in);


    fwrite(&header, 1, sizeof(header), out);

    int bytesPerSample = header.bitsPerSample / 8;
    uint32_t samplesPerChannel = header.subchunk2Size / (header.numChannels * bytesPerSample);

    for (uint32_t i = 0; i < samplesPerChannel; ++i) {
        for (int j = 0; j < header.numChannels; ++j) {
            int8_t sample[4];
            fread(sample, 1, bytesPerSample, in);

            if (j == channel_to_mute) {
                for (int k = 0; k < bytesPerSample; ++k) {
                    sample[k] = 0;
                }
                replaced_samples++;
            }

            fwrite(sample, 1, bytesPerSample, out);
        }
    }

    fclose(in);
    fclose(out);

    return replaced_samples;
}
