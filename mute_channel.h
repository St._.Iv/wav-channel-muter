#ifndef MUTE_CHANNEL_H
#define MUTE_CHANNEL_H

int mute_channel(const char *input_file, const char *output_file, int channel_to_mute, int silent);

#endif